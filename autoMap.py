#!/usr/bin/env python
#This is still a work in progress!

import sys
import subprocess
#Main function to gather optoins, etc
def main():
	#ASCII arts & show/get options from user
	print " ############################################ "
	print " ############################################ "
	print " ######                                ###### "
	print " ######     autoMap: nmap automation   ###### "
	print " ######              v.2               ###### "
	print " ######        matt@withani.net        ###### "
	print " ######                                ###### "
	print " ############################################ "
	print ""
	print " Target Options: "
	print " 1. Scan Entire /24 Subnet"
	print " 2. Scan Entire /16 Subnet"
	print " 3. Scan List of IP Addresses"
	print " 4. Scan Single IP Address"
	print " 0. Exit"
	print ""
	#Get Target address(es) from user
	toption = raw_input("Select a Target Option: ")
	#Check is Exit is triggered
	if toption == "0":
		sys.exit()

	print ""
	print " Scan Options: "
	print " 1. Ping Scan (-sn [ip address])"
	print " 2. Standard Port Scan (-T4 -A -v [ip address])"
	print " 3. Intense Port Scan (-p 1-65535 -T4 -A -v [ip address])"
	print " 0. Exit"
	print ""
	#Get Scan Option from user
	soption = raw_input("Select a Scan Option: ")
	#Check if Exit is triggered
	if soption == "0":
		sys.exit()

	#If Target Option 1 is entered get /24 range
	if toption == "1":
		ip = raw_input("Enter the /24 range you would like to scan (i.e: 192.168.1.0): ")
		ip = ip + "/24"
		sType(soption,ip)

	#If Target Option 2 is entered get /16 range
	elif toption == "2":
		ip = raw_input("Enter the /16 range you would like to scan (i.e: 192.168.0.0): ")
		ip = ip + "/16"
		sType(soption,ip)

	#If Target Option 3 is entered get IP Address list
	elif toption == "3":
		ipfile = raw_input("Enter the path to the IP Address list: ")
		ipfile = open(ipfile).readlines()
		for ip in ipfile:
			sType(soption,ip)

	#If Target Option 4 is entered get single IP address
	elif toption == "4":
		ip = raw_input("Enter the IP Address you would like to scan: ")
		sType(soption,ip)

	#If Target Option 0 is entered
	elif toption == "0":
		sys.exit()

#Determine Scan Type Function
def sType(soption,ip):
	 #If Scan Option 1 is entered do Ping Scan
        if soption == "1":
                return pingScan(ip)
        
        #If Scan Option 2 is entered do Standard Port Scan
        elif soption == "2":
		return standardScan(ip)        

        #If Scan Option 3 is entered do Intense Port Scan
        elif soption == "3":
                return intenseScan(ip)

	#If Scan Option - is entered do Exit
	elif soption == "0":
		sys.exit()

        #Catch if invalid option is entered
        else:
                print "Invalid option entered.  Please enter a valid Scan Option next time."
                raw_input("Press any key to return to the main menu.")
                main()

#Ping Scan Function
def pingScan(ip):
	outputfile = ip.replace('.','-').replace('/','').strip() + '-ping-scan'
	ipin = ip.strip()
	print "Scanning %s..." % (ipin)
	subprocess.Popen("nmap -sn %s >> %s" % (ipin,outputfile), shell=True).wait()

#Standard Port Scan Function
def standardScan(ip):
	outputfile = ip.replace('.','-').replace('/','').strip() + '-standard-scan'
	ipin = ip.strip()
	print "Scanning %s..." % (ipin)
	subprocess.Popen("nmap -T4 -A -v %s >> %s" % (ipin,outputfile), shell=True).wait()

#Intense Port Scan Function
def intenseScan(ip):
	outputfile = ip.replace('.','-').replace('/','').strip() + '-intense-scan'
	ipin = ip.strip()
	print "Scanning %s..." % (ipin)
	subprocess.Popen("nmap -p 1-65535 -T4 -A -v %s >> %s" % (ipin,outputfile), shell=True).wait()
main()